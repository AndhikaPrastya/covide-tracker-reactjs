import {useState,React} from "react";
import { Container, Row, Col, Card } from "react-bootstrap";
import CardComponent from "./components/Card";
import NavbarComponent from "./components/Navbar";
import ListCountryComponent from "./components/ListCountries";
import CardCountryComponent from "./components/CardCountry";
import covideImage from "../src/assets/image.jpg"
import "./style/myStyle.css"
import "./App.css"

function App() {
  const [selectCountrie, setSelectCountrie] = useState('')
  function handleChange(value) {
    setSelectCountrie(value)
  }
  return (
    <div>
      <div className="headerBG">
        <NavbarComponent/>
        <CardComponent/>
          <Container className="d-flex mainInfo" >
            <Row className='listCountry'>
              <ListCountryComponent handleChange={handleChange}/>
              <Col sm={10} xs="auto">
                <Row className='d-flex justify-content-center'>
                    <Card.Img className='mainImage' src={covideImage} alt='Covide Image'/>
                </Row>
                <Row className='d-flex align-items-end'>
                    <CardCountryComponent country={selectCountrie}/>
                </Row>
              </Col>
            </Row>
          </Container>
      </div>
    </div>
  );
}

export default App;
