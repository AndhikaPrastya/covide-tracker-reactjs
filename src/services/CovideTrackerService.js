import axios from "axios";
import { baseUrl, apiKey, apiHost } from '../constant/Constant'

// ** React Imports
// import { Fragment, React } from 'react'

export default class CovideTrackerService {
    
    // async getCountries(search) {
    //     let params = ""
    //     if (search !== " " && search !== "") {
    //         params += `?search=${search}`
    //     }

    //     return this.fetch(`${baseUrl}/countries${params}`, {
    //         method: 'GET'
    //     }).then(async (response) => {
    //         const {data, message, statusCode, error} = await response.json()
    //         console.log('test', response)
    //         return Promise.resolve({data, message, statusCode, error})
    //     })
    // }

    async getCountries(search) {
        let params = ""
        if (search !== " " && search !== "") {
            params += `?search=${search}`
        }

        // return this.fetch(`${baseUrl}/countries${params}`, {
        //     method: 'GET'
        // }).then(async (response) => {
        //     // console.log('test', response.json())
        //     const {data, status} = await response.json()
        //     console.log('test', response)
        //     return Promise.resolve({data, status})
        // })

        let config = {
            method: 'get',
            url: `${baseUrl}/countries${params}`,
            headers: { 
              'X-RapidAPI-Key': `${apiKey}`, 
              'X-RapidAPI-Host': `${apiHost}`
            }
          };
          
          await axios(config)
            .then(async function (response) {
                console.log(response)
                const data = await response
                return Promise.resolve(data)
                // return await response
            })
            .catch(async function (error) {
                console.log(error);
            });

    }

    async fetch(url, options) {
        // performs api calls sending the required authentication headers
        const headers = {
            'X-RapidAPI-Key': `${apiKey}`,
            'X-RapidAPI-Host': `${apiHost}`
        }

        return fetch(url, {
            headers,
            ...options
        }).then(response => {
            return response
        })
    }

}

