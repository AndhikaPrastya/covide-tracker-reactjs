import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';

function NavbarComponent() {
  return (
    <Navbar variant="light">
      <Container>
        <Navbar.Brand className='text-white'>
            <div className='title'>Covide Tracker</div>
        </Navbar.Brand>
      </Container>
    </Navbar>
  );
}

export default NavbarComponent;