import { Container, Row, Col, Card } from "react-bootstrap";
import { baseUrl, apiKey, apiHost } from '../constant/Constant'
import axios from "axios";
import {useState, useEffect, React } from 'react'
import "../style/myStyle.css"

function CardCountryComponent(props) {
    const [dataHistory, setData] = useState({
        confirmed: {
            title : 'CONFIRMED',
            value: 0
        },
        recovered: {
            title : 'RECOVERED',
            value: 0
        },
        critical: {
            title : 'CRITICAL',
            value: 0
        },
        death: {
            title : 'DEATH',
            value: 0
        },
      });

    const fetchData  = async (country) => {   
        console.log(props.country)
        let params = ""
        if (country !== " " && country !== "") {
            params += `&country=${country}`
        }
        let config = {
              method: 'get',
              url: `${baseUrl}/history?${params}`,
              headers: { 
                'X-RapidAPI-Key': `${apiKey}`, 
                'X-RapidAPI-Host': `${apiHost}`
              }
            };
            if(country !== " " && country !== ""){
                await axios(config)
                    .then(async function (response) {
                        const newData = { ...dataHistory }
                          newData['confirmed']['value'] = response.data.response[0].cases.active
                          newData['recovered']['value'] = response.data.response[0].cases.recovered
                          newData['critical']['value'] = response.data.response[0].cases.critical
                          newData['death']['value'] = response.data.response[0].deaths.total
                          setData(newData)
                    })
                    .catch(async function (error) {
                        console.log(error);
                    });
            }
    }
    
    useEffect(() => {
        fetchData(props.country)
    })

  return (
    <Container className='d-flex justify-content-center align-item-center'>
            <Row>
              <Col className='cardCountry'>
                <Card className="boxCountry">
                    <Card.Body>
                        <Card.Title className="fontCountryTitle text-center">{dataHistory.confirmed.title}</Card.Title>
                        <Card.Text className="fontCountry text-center">{dataHistory.confirmed.value.toLocaleString()}</Card.Text>
                    </Card.Body>
                </Card>
              </Col>
              <Col className='cardCountry'>
                <Card className="boxCountry">
                    <Card.Body>
                        <Card.Title className="fontCountryTitle text-center">{dataHistory.recovered.title}</Card.Title>
                        <Card.Text className="fontCountry text-center">{dataHistory.recovered.value.toLocaleString()}</Card.Text>
                    </Card.Body>
                </Card>
              </Col>
              <Col className='cardCountry'>
                <Card className="boxCountry">
                    <Card.Body>
                        <Card.Title className="fontCountryTitle text-center">{dataHistory.critical.title}</Card.Title>
                        <Card.Text className="fontCountry text-center">{dataHistory.critical.value.toLocaleString()}</Card.Text>
                    </Card.Body>
                </Card>
              </Col>
              <Col className='cardCountry'>
                <Card className="boxCountry">
                    <Card.Body>
                        <Card.Title className="fontCountryTitle text-center">{dataHistory.death.title}</Card.Title>
                        <Card.Text className="fontCountry text-center">{dataHistory.death.value.toLocaleString()}</Card.Text>
                    </Card.Body>
                </Card>
              </Col>
            </Row>
    </Container>
  );
}

export default CardCountryComponent;