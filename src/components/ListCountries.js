import {Col, Form, ListGroup, Container } from "react-bootstrap";
import { baseUrl, apiKey, apiHost } from '../constant/Constant'
import axios from "axios";
import {useState, useEffect, React } from 'react'
import "../style/myStyle.css"

function ListCountryComponent(props) {
    const [dataCountry, setData] = useState([])
    const [keywords, setKeywords] = useState('')

    const fetchData  = async (search) => {    
      let params = ""
      if (search !== " " && search !== "") {
          params += `search=${search}`
      }
        let config = {
              method: 'get',
              url: `${baseUrl}/countries?${params}`,
              headers: { 
                'X-RapidAPI-Key': `${apiKey}`, 
                'X-RapidAPI-Host': `${apiHost}`
              }
            };
          await axios(config)
              .then(async function (response) {
                    setData(response.data.response)
              })
              .catch(async function (error) {
                  console.log(error);
              });
    }

    const handleSubmit = e => {
      e.preventDefault()
      fetchData(keywords)
    }

    const handleClick = (value) => {
      props.handleChange(value)
    }
    
    useEffect(() => {
      fetchData('')
    }, [])

  return (
    <Col className='sideBar' >
        <div className="App">
         <Form onSubmit={handleSubmit}>
            <Form.Group className="mb-3">
                <Form.Control placeholder="Search Country" 
                              type="text" value={keywords}
                              onChange={e => setKeywords(e.target.value)} />
            </Form.Group>
         </Form> 
         <Container className="containerScroll border">
            {dataCountry.map((val, key)=>{
                return <ListGroup >
                            <ListGroup.Item className="listItem text-start"  action onClick={()=>{handleClick(val)}}>{val}</ListGroup.Item>
                        </ListGroup> 
            })}  
         </Container>
        </div>
    </Col>
  );
}

export default ListCountryComponent;