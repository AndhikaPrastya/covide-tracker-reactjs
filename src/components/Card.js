import { Container, Row, Col, Card } from "react-bootstrap";
import { baseUrl, apiKey, apiHost } from '../constant/Constant'
import icon1 from "../assets/icon1.png";
import icon2 from "../assets/icon2.png";
import icon3 from "../assets/icon3.png";
import icon4 from "../assets/icon4.png";
import axios from "axios";
import moment from 'moment';
import {useState, useEffect, React } from 'react'
import "../style/myStyle.css"

function CardComponent() {
    const [dataHistory, setData] = useState({
        confirmed: {
            title : 'CONFIRMED',
            value: 0
        },
        recovered: {
            title : 'RECOVERED',
            value: 0
        },
        critical: {
            title : 'CRITICAL',
            value: 0
        },
        death: {
            title : 'DEATH',
            value: 0
        },
      });

    const fetchData  = async (country) => {
        var now = new Date();
        var date = moment(now).format('YYYY-MM-DD');      
        let params = ""
        if (country !== " " && country !== "") {
            params += `&country=${country}`
        }
        if (date !== " " && date !== "") {
            params += `&day=${date}`
        }
        let config = {
              method: 'get',
              url: `${baseUrl}/history?${params}`,
              headers: { 
                'X-RapidAPI-Key': `${apiKey}`, 
                'X-RapidAPI-Host': `${apiHost}`
              }
            };
          await axios(config)
              .then(async function (response) {
                  const newData = { ...dataHistory }
                    newData['confirmed']['value'] = response.data.response[0].cases.active
                    newData['recovered']['value'] = response.data.response[0].cases.recovered
                    newData['critical']['value'] = response.data.response[0].cases.critical
                    newData['death']['value'] = response.data.response[0].deaths.total
                    setData(newData)
              })
              .catch(async function (error) {
                  console.log(error);
              });
    }
    
    useEffect(() => {
        fetchData('all')
      })

  return (
    <Container className='d-flex justify-content-center'>
            <Row>
              <Col className='cardInfo'>
                <Card className="boxInfo">
                    <Card.Body>
                        <Row>
                            <Col>
                                <Card.Title className='fontCardTitle'>{dataHistory.confirmed.title}</Card.Title>
                                <Card.Text className='fontCard'>{dataHistory.confirmed.value.toLocaleString()}</Card.Text>
                            </Col>
                            <Col>
                                <Card.Img className='icon' src={icon1} alt='icon1'/>
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>
              </Col>
              <Col className='cardInfo'>
                <Card className="boxInfo">
                    <Card.Body>
                        <Row>
                            <Col>
                                <Card.Title className='fontCardTitle'>{dataHistory.recovered.title}</Card.Title>
                                <Card.Text className='fontCard'>{dataHistory.recovered.value.toLocaleString()}</Card.Text>
                            </Col>
                            <Col>
                                <Card.Img className='icon' src={icon2} alt='icon1'/>
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>
              </Col>
              <Col className='cardInfo'>
                <Card className="boxInfo">
                    <Card.Body>
                        <Row>
                            <Col>
                                <Card.Title className='fontCardTitle'>{dataHistory.critical.title}</Card.Title>
                                <Card.Text className='fontCard'>{dataHistory.critical.value.toLocaleString()}</Card.Text>
                            </Col>
                            <Col>
                                <Card.Img className='icon' src={icon3} alt='icon1'/>
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>
              </Col>
              <Col className='cardInfo'>
                <Card className="boxInfo">
                    <Card.Body>
                        <Row>
                            <Col>
                                <Card.Title className='fontCardTitle'>{dataHistory.death.title}</Card.Title>
                                <Card.Text className='fontCard'>{dataHistory.death.value.toLocaleString()}</Card.Text>
                            </Col>
                            <Col>
                                <Card.Img className='icon' src={icon4} alt='icon1'/>
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>
              </Col>
            </Row>
    </Container>
  );
}

export default CardComponent;